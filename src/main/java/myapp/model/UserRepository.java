package myapp.model;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    public Optional<User> findByUsernameAndPassword(String username, String password);

    @Query("SELECT new myapp.model.Credentials(username,password) " +
            "FROM User WHERE username=:user AND password=:pass")
    public List<Credentials> getCredentials(@Param("user") String username, @Param("pass") String password);
}
