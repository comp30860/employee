package myapp.model;

public class DepartmentData {
    private Long id;
    private String name;
    private long count;

    public DepartmentData(Long id, String name, long count) {
        this.id = id;
        this.name = name;
        this.count = count;
    }

    public DepartmentData(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public long getCount() {
        return count;
    }
}