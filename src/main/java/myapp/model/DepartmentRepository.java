package myapp.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {
    @Query("SELECT new myapp.model.DepartmentData(d.id,d.name,COUNT(e)) FROM Department d JOIN Employee e ON d.id=e.department GROUP BY d.id")
    public List<DepartmentData> getDepartments();
}
