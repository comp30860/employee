package myapp.controller;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import myapp.model.User;
import myapp.model.UserData;
import myapp.model.UserRepository;

@Controller
public class AuthenticationController {
    @Autowired
    UserData userData;

    @Autowired
    UserRepository userRepository;

    @GetMapping("/login")
    public String loginView(Model model) {
        model.addAttribute("user", userData.getUser());
        return "login";
    }

    @PostMapping("/login")
    public void login(String username, String password, HttpServletResponse response) throws IOException {
        Optional<User> result = userRepository.findByUsernameAndPassword(username, password);
        if (result.isPresent()) {
            userData.setUser(result.get());
            response.sendRedirect("/home");
        } else {
            response.sendRedirect("/invalidUser");
        }
    }

    @GetMapping("/invalidUser")
    public String failureView() {
        return "failure";
    }

    @GetMapping("/logout")
    public void logout(HttpServletResponse response) throws IOException {
        userData.setUser(null);
        response.sendRedirect("/");
    }
}