package myapp.controller;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import myapp.model.Department;
import myapp.model.DepartmentRepository;
import myapp.model.Employee;
import myapp.model.EmployeeRepository;
import myapp.model.UserData;

@Controller
public class HomeController {
    @Autowired
    private UserData userdata;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    private boolean isLoggedIn() {
        return userdata.getUser() != null;
    }

    @GetMapping("/")
    public void index(HttpServletResponse response) throws IOException {
        if (isLoggedIn())
            response.sendRedirect("/login");
        else
            response.sendRedirect("/home");
    }

    @GetMapping("/home")
    public String home(Model model, HttpServletResponse response) throws IOException {
        if (!isLoggedIn()) {
            response.sendRedirect("/login");
            return null;
        }

        model.addAttribute("departments", departmentRepository.getDepartments());
        model.addAttribute("user", userdata.getUser());
        return "content/home.html";
    }

    @GetMapping("/department")
    public String department(@RequestParam("id") long id, Model model, HttpServletResponse response) throws IOException {
        if (!isLoggedIn()) {
            response.sendRedirect("/login");
            return null;
        }
        Optional<Department> department = departmentRepository.findById(id);
        model.addAttribute("department", department.get());
        model.addAttribute("user", userdata.getUser());
        return "content/department";
    }

    @GetMapping("/employee")
    public String employee(@RequestParam("id") long id, Model model, HttpServletResponse response) throws IOException {
        if (!isLoggedIn()) {
            response.sendRedirect("/login");
            return null;
        }
        Optional<Employee> employee = employeeRepository.findById(id);
        model.addAttribute("employee", employee.get());
        model.addAttribute("user", userdata.getUser());
        return "content/employee";
    }
}