INSERT INTO users(id, username, password, full_Name, email) VALUES (1, 'rem','sausage', 'Rem', 'rem.collier@ucd.ie');
INSERT INTO department(id, name) VALUES(0, 'IT'), (1, 'HR');
INSERT INTO employee(id, name, age, department_id) VALUES(0, 'rem', 46, 0), (1, 'eoin', 22, 0), (2, 'george', 23, 1);
INSERT INTO phone(id, employee_id, number) VALUES(0, 0, '017162465'), (1, 0, '0871231234');